 ###############################################################################################
## Title: Does rearing environment affect learning in a group-living lizard?
## Authors: Julia Riley, Anna Kuechler, Theo Damais, Dan Noble, Richard Bryne, Martin Whiting
## Date: 16.11.2016
## Notes: NA
###############################################################################################

# ~~~~~~~~~~~~~~~~~~~~~~~~~
# Information on Reversal
# ~~~~~~~~~~~~~~~~~~~~~~~~~
# Learning criteria: 7/8 (start on correct)
# Ntrials: 34
# Nlizards: 29 
# Nmom: 19
# Ntub: 25


## Load data
## 1)
liz_dat<-read.csv("data/all_trials_by_liz.csv", stringsAsFactors = TRUE, header = TRUE)
str(liz_dat)
liz_dat$mom_id<-as.factor(liz_dat$mom_id)

## 2)
trial_dat<-read.csv("data/reverse_by_trial.csv", stringsAsFactors = TRUE, header = TRUE)
str(trial_dat)
trial_dat$mom_id<-as.factor(trial_dat$mom_id)


#########################
## INDIVIDUAL-BASED DATA
#########################

# Response Variables

# (1) learn_bi -> binomial
# (2) trial until learnt -> poisson

# Predictor Variables
# - learning treatment
# - social treatment

# Random effects 
# - mom_id
# - tub


## DATA EXPLORATION (as per Zurr et al.)
# --------------------------------------
# A Are there missing values?
# B Are there outliers?
# C Is there collineratiry (X)?
# D Relationships Y vs. X
# E Spatial/temporal aspects of sampling design 
# F Interactions
# G Zero inflation Y


#A. Are there missing values?
liz_rev<-liz_dat[liz_dat$trial=="reverse",]
colSums(is.na(liz_rev)) # 19 lizards didn't learn 


#B. Are there outliers?
source(file="support/HighstatLibV10.R")
#reponse variable 1
dotchart(liz_rev$learn_bi, xlab = 'Range of Prob Learn', ylab = 'Order of the data')
#reponse variable 2
dotchart(liz_rev$num_trials, xlab = 'Range of Num Trials to Learn', ylab = 'Order of the data') 
min(liz_rev$num_trials, na.rm=TRUE) 

# predictor variable
table(liz_rev$soc_treat) #P:15, S:14
table(liz_rev$learn_treat) #CL: 15, SL:14

# random effects even?
table(liz_rev$mom_id) # varies from 1-3
table(liz_rev$tub)
liz_rev$tub<-droplevels(liz_rev$tub) #remove 0s
table(liz_rev$tub) # varies from 1-2


#C Is there collineratiry (X)? #NA


# D Relationships Y vs. X
# resp. 1
boxplot(learn_bi~soc_treat, data = liz_rev) 
boxplot(learn_bi~learn_treat, data = liz_rev) 

# resp. 2
boxplot(num_trials~soc_treat, data = liz_rev)
boxplot(num_trials~learn_treat, data = liz_rev)


# E Spatial/temporal aspects of sampling design #NA


# F Interactions #NA


# G Zero inflation Y
sum(liz_rev$learn_bi == 0, na.rm=TRUE)
100 * sum(liz_rev$learn_bi == 0, na.rm=TRUE) / nrow(liz_rev) # NA because Binomial

sum(liz_rev$num_trials == 0, na.rm=TRUE)
100 * sum(liz_rev$num_trials == 0, na.rm=TRUE) / nrow(liz_rev) # 0



#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

## Does the number of lizards that learnt the instru task vary between soc_treat, learn_treat?

# general summary
str(liz_rev)
P_frame<-liz_rev[c("learn", "learn_treat", "soc_treat")]
library(plyr)
P_frame<-ddply(P_frame, c('learn_treat',"soc_treat","learn"))
P_frame
# overall 10/29 lizards learnt the reversal task
# Socially-raised lizards - 3/8 CL learnt the instru task
#                         - 1/7 SL learnt the instru task

# Isolated lizards - 2/7 CL learnt the instru task
#                  - 4/7 SL learnt the instru task


#statistical model
library(lme4)

rev.M1.F<-glmer(learn_bi~soc_treat+learn_treat +(1|mom_id)+(1|tub), family=binomial, data=liz_rev)
summary(rev.M1.F)
drop1(rev.M1.F, test = "Chi") 
# social & learn treatment are nonsignificant


# effect sizes
# rearing treatment with learning treatment marginalized
rear.eff.1<-effects::Effect(focal.predictors = "soc_treat", mod = rev.M1.F, latent=FALSE, se = TRUE, confidence.level = 0.95) 
summary(rear.eff.1)

# learning treatment with rearing treatment marginalized
learn.eff.1<-effects::Effect(focal.predictors = "learn_treat", mod = rev.M1.F, latent=FALSE, se = TRUE, confidence.level = 0.95) 
summary(learn.eff.1)



## Does the trials taken to learn the instru task vary between soc_treat, learn_treat?

# general summary
source(file="support/func_summarySE.R")
summarySE(liz_rev, measurevar="num_trials", groupvars=c("soc_treat", "learn_treat"), na.rm=TRUE) 
#soc_treat learn_treat N num_trials       sd       se        ci
#1         P          CL 3   22.33333 3.511885 2.027588  8.724005
#2         P          SL 1   30.00000       NA       NA        NA
#3         S          CL 2   27.50000 7.778175 5.500000 69.884126
#4         S          SL 4   23.50000 5.744563 2.872281  9.140881


liz_rev_na<-na.omit(liz_rev) #remove NAs
colSums(is.na(liz_rev_na))


# statistical model
rev.M2.F<-glmer(num_trials ~ soc_treat + learn_treat +(1|mom_id) + (1|tub), family=poisson, data=liz_rev_na)
summary(rev.M2.F)
drop1(rev.M2.F, test = "Chi")
# social & learning treatment nonsignificant


# effect sizes
# rearing treatment with learning treatment marginalized
rear.eff.2<-effects::Effect(focal.predictors = "soc_treat", mod = rev.M2.F, latent=FALSE, se = TRUE, confidence.level = 0.95) 
summary(rear.eff.2)

# learning treatment with rearing treatment marginalized
learn.eff.2<-effects::Effect(focal.predictors = "learn_treat", mod = rev.M2.F, latent=FALSE, se = TRUE, confidence.level = 0.95) 
summary(learn.eff.2)



#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

############################
## REPEATED DATA ACROSS TIME
############################

# Response Variables
# (1) in_correct

# Predictor Variables
# - trial_num
# - learn_treat
# - soc_treat

# Random effects 
# - mom_id (random intercept)
# - liz_id (random slope & intercept)
# - tub (random intercept)



## DATA EXPLORATION (as per Zurr et al.)
# --------------------------------------
# A Are there missing values?
# B Are there outliers?
# C Is there collineratiry (X)?
# D Relationships Y vs. X
# E Spatial/temporal aspects of sampling design 
# F Interactions
# G Zero inflation Y


#A. Are there missing values?
colSums(is.na(trial_dat)) 


#B. Are there outliers?
source(file="support/HighstatLibV10.R")
#reponse variable 1
dotchart(trial_dat$in_correct, xlab = 'Range of Prob Learn', ylab = 'Order of the data') #no outliers

# predictor variable
table(trial_dat$soc_treat) #P:510, S:476
table(trial_dat$learn_treat) #CL:510, SL:476

# random effects even?
table(trial_dat$mom_id) # varies from 34-102
table(trial_dat$liz_id) # all 24
table(trial_dat$tub) # varies from 34-68


#C Is there collineratiry (X)? #NA


# D Relationships Y vs. X
# resp. 1
boxplot(in_correct~soc_treat, data = trial_dat)
boxplot(in_correct~learn_treat, data = trial_dat)


# E Spatial/temporal aspects of sampling design #NA


# F Interactions 
#social treatment:trial number
coplot(in_correct ~ trial_num | soc_treat,
       data = trial_dat,
       panel = function(x, y, ...) {
         tmp <- lm(y ~ x, na.action = na.omit)
         abline(tmp)
         points(x, y) })

#learning treatment:trial number
coplot(in_correct ~ trial_num | learn_treat,
       data = trial_dat,
       panel = function(x, y, ...) {
         tmp <- lm(y ~ x, na.action = na.omit)
         abline(tmp)
         points(x, y) })


# G Zero inflation Y
sum(trial_dat$in_correct == 0, na.rm=TRUE) #586, but NA because Binomial



## Does the probability of learning that vary between soc_treat, learn_treat?
library(lme4)

length(trial_dat$in_correct) #986


# statistical model
rev.M3.1<-glmer(in_correct ~ trial_num + soc_treat + learn_treat + trial_num:soc_treat + trial_num:learn_treat +
                (0+trial_num|liz_id) + (1|liz_id) + (1|mom_id) + (1|tub), family=binomial, data=trial_dat,
                control=glmerControl(optCtrl=list(maxfun=4e4))) # did not converge

ss <- getME(rev.M3.1, c("theta","fixef")) #step to try to help with convergence

rev.M3.2<-glmer(in_correct ~ trial_num + soc_treat + learn_treat + trial_num:soc_treat + trial_num:learn_treat +
                (0+trial_num|liz_id) + (1|liz_id) + (1|mom_id) + (1|tub), family=binomial, data=trial_dat,
                control=glmerControl(optCtrl=list(maxfun=4e4)), start=ss)

summary(rev.M3.2)
#interaction between trial number & learning treatment not significant (remove)
#interaction between trial number & rearing treatment not significant (remove)

rev.M3.F<-glmer(in_correct ~ trial_num + soc_treat + learn_treat +
                (0+trial_num|liz_id) + (1|liz_id) + (1|mom_id) + (1|tub), family=binomial, data=trial_dat)

summary(rev.M3.F)
drop1(assoc.M3.F, test = "Chi") 
# social & learning treatment not significant


# effect sizes
# rearing treatment with learning treatment marginalized
rear.eff.3<-effects::Effect(focal.predictors = "soc_treat", mod = rev.M3.F, latent=FALSE, se = TRUE, confidence.level = 0.95) 
summary(rear.eff.3)

# learning treatment with rearing treatment marginalized
learn.eff.3<-effects::Effect(focal.predictors = "learn_treat", mod = rev.M3.F, latent=FALSE, se = TRUE, confidence.level = 0.95) 
summary(learn.eff.3)



#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~